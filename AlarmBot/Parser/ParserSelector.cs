﻿using AlarmBot.Data;
using AlarmBot.Parser.DrkBos;
using NLog;
using System;

namespace AlarmBot.Parser
{
    public class ParserSelector
    {
        private static ILogger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="basicData"></param>
        /// <returns></returns>
        public static IAlarmData RunParser(string parserName, IAlarmData basicData)
        {
            if(string.IsNullOrEmpty(parserName))
            {
                return basicData;
            }

            try
            {
                switch (parserName.ToLower())
                {
                    case "hvo":
                        return new HvOParser().Parse(basicData);

                    default:
                        log.Info($"Kein spezifischer Parser fuer Adresse {basicData.Alarmadresse} konfiguriert.");
                        return basicData;
                }
            }
            catch(Exception e)
            {
                log.Error(e, "Fehler beim erweiterten Parsen der Alarmdaten");
                return basicData;
            }
        }
    }
}
