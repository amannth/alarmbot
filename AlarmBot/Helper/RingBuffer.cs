﻿using NLog;
using System.Collections.Generic;

namespace AlarmBot.Helper
{
    /// <summary>
    /// Implementiert einen Ringpuffer. Anzal der Elemnte wird im Konstruktor uebergeben
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RingBuffer<T>
    {
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly int bufferSize;
        private int slotPointer = 0;

        /// <summary>
        /// Zugriff auf die Elemente des Puffers
        /// </summary>
        public List<T> Buffer { get;}

        /// <summary>
        /// ERstellen einer neuen Instanz des Ringpuffers
        /// </summary>
        /// <param name="size">Maximale Anzal der Elemente im Puffer</param>
        public RingBuffer(int size)
        {
            Buffer = new List<T>(size);
            bufferSize = size;
        }

        /// <summary>
        /// Ein Element den Ringpuffer hinzufuegen
        /// </summary>
        /// <param name="data"></param>
        public void Push(T data)
        {
            if (slotPointer >= bufferSize)
            {
                logger.Debug("Resetting slotPointer");
                slotPointer = 0;
            }

            if (Buffer.Count < bufferSize)
            {
                logger.Debug($"Adding data to ringbuffer slot {slotPointer}");
                Buffer.Add(data);
                slotPointer++;
            }
            else
            {
                logger.Debug($"Adding data to ringbuffer slot {slotPointer}");
                Buffer[slotPointer] = data;
                slotPointer++;
            }
        }
    }
}
