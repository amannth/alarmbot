﻿using AlarmBot.Data;
using NLog;

namespace AlarmBot.Persistence
{
    /// <summary>
    /// Implementierung von <see cref="IAlarmDataWriter"/> welche in ein Logfile via NLog schreibt
    /// </summary>
    public class LogFileWriter : IAlarmDataWriter
    {
        private readonly Logger log = LogManager.GetLogger("AlarmLogger");

        public void Init()
        {
        }

        /// <summary>
        /// Empfangenes Alarmdata objekt in ein spezifisches Logfile schreiben
        /// </summary>
        /// <param name="data"></param>
        public void Write(IAlarmData data)
        {
            log.Info(data.ToLogString());
        }
    }
}
