﻿using System;
using System.Runtime.Serialization;

namespace AlarmBot.Filter
{
    public class AlarmFilterException : Exception
    {
        public AlarmFilterException()
        {
        }

        public AlarmFilterException(string message) : base(message)
        {
        }

        public AlarmFilterException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AlarmFilterException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
