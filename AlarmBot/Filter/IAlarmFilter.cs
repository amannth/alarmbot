﻿using AlarmBot.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlarmBot.Filter
{
    public interface IAlarmFilter
    {
        bool Excecute(IAlarmData data, List<IAlarmData> alarmBuffer);
    }
}
