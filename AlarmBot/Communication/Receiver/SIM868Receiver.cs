﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using AlarmBot.Data;
using AlarmBot.Parser;
using NLog;

namespace AlarmBot.Communication.Receiver
{
    /// <summary>
    /// Receiver klasse fuer SIM868 GSM Modul
    /// <seealso cref="https://www.waveshare.com/wiki/GSM/GPRS/GNSS_HAT"/>
    /// <seealso cref="https://www.waveshare.com/w/upload/4/4a/GSM_GPRS_GNSS_HAT_User_Manual_EN.pdf"/>
    /// </summary>
    public class SIM868Receiver : IAlarmReceiver
    {
        /// <summary>
        /// Konstantes Ende Zeichen der Meldungsausgabe
        /// Der BOSS92x sendet immer null-terminierte strings
        /// </summary>
        private const byte EOT = 0x1A;

        private readonly Logger log = LogManager.GetCurrentClassLogger();
        private readonly Logger serialLog = LogManager.GetLogger("SerialLogger");
        private readonly Stopwatch sw = new Stopwatch();
        private readonly TimeSpan timeout = new TimeSpan(0, 0, 2);
        private readonly SerialPort serialPort;

        public IBasicAlarmDataParser DataParser { get; set; }

        public event Action<string, Exception> ErrorOccured;
        public event Action<IAlarmData> AlarmReceived;

        public SIM868Receiver(string comPort, IBasicAlarmDataParser parser)
        {
            serialPort = new SerialPort(comPort, 115200, Parity.None);
            serialPort.ErrorReceived += SerialPort_ErrorReceived;
            serialPort.DataReceived += SerialPort_DataReceived;
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            log.Info("Receiving serial data...");
            List<int> serialData = new List<int>();
            bool isEot = false;

            sw.Start();

            while (!isEot)
            {
                if (sw.Elapsed > timeout)
                {
                    serialLog.Warn("Received invalid data in timeout 2s: " + Environment.NewLine + serialData);
                    serialPort.ReadExisting();
                    sw.Stop();
                    sw.Reset();
                    Console.WriteLine();
                    return;
                }

                while (serialPort.BytesToRead > 0)
                {
                    int dataByte = serialPort.ReadByte();
                     
                    if (dataByte == EOT)
                    {
                        isEot = true;
                        break;
                    }
                    serialData.Add(dataByte);
                }
            }
            sw.Stop();

            Console.WriteLine();
        }

        private void SerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void Connect()
        {
            serialPort.Open();


            // send at to check connection
            serialPort.WriteLine("AT");
            // switch on with raspi gipo

            // wait untill at = ok


            // switch to text mode : AT+CMGF=1
            //serialPort.WriteLine("AT+CMGF=1");

        }
    }
}
