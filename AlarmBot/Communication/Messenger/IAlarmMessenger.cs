﻿using AlarmBot.Data;
using System.Threading.Tasks;

namespace AlarmBot.Communication.Sender
{
    public interface IAlarmMessenger
    {
        Task SendAlarm(IAlarmData alarmData);
        Task SendAdminMessage(string message);
    }
}
