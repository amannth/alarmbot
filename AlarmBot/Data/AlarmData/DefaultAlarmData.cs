﻿using System;
using System.Text;

namespace AlarmBot.Data
{
    public class DefaultAlarmData : IAlarmData
    {
        /// <summary>
        /// Zeitpukt zu welchem der Alarm von AlarmBot empfangen wurde
        /// </summary>
        public DateTime Empfangszeit { get; }

        /// <summary>
        /// Zeitstempel aus der Alarm Meldung
        /// </summary>
        public DateTime Alarmzeit { get; }

        /// <summary>
        /// Welche Alarmadresse inerhalb des Empfangsgeraetes (z.B. FME BOSS925) 
        /// wurde alarmiert (z.B. 12B beim BOS925). Dies ist NICHT die RIC!!
        /// </summary>
        public string Alarmadresse { get; }

        /// <summary>
        /// Der Fixtext zum Alarm (aus Config datei oder Melderprogrammierung)
        /// </summary>
        public string Alarmtext { get; }

        /// <summary>
        /// Der eigentliche, unbearbeitete Meldungstext der Leitstelle
        /// </summary>
        public string MeldungstextRaw { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="empfangsZeit"></param>
        /// <param name="alarmZeit"></param>
        /// <param name="alarmadresse"></param>
        /// <param name="alarmtext"></param>
        /// <param name="meldungstextRaw"></param>
        public DefaultAlarmData(DateTime empfangsZeit, DateTime alarmZeit, string alarmadresse, string alarmtext, string meldungstextRaw)
        {
            Empfangszeit = empfangsZeit;
            Alarmzeit = alarmZeit;
            Alarmadresse = alarmadresse;
            Alarmtext = alarmtext;
            MeldungstextRaw = meldungstextRaw;
        }

        /// <summary>
        /// Ausgabestring fuer ein Logfile erstellen
        /// </summary>
        /// <returns></returns>
        public virtual string ToLogString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("--- DefaultAlarmData ---");
            sb.AppendLine($"Alarmzeit: {Alarmzeit}");
            sb.AppendLine($"Empfangszeit: {Empfangszeit}");
            sb.AppendLine($"Alarmadresse: {Alarmadresse}");
            sb.AppendLine($"Alarmtext: {Alarmtext}");
            sb.AppendLine($"MeldungstextRaw: {MeldungstextRaw}");

            return sb.ToString();
        }


        /// <summary>
        /// Ausgabestring (ohne Leitstellenmeldung) zur Verteilung an die Helfer erstellen
        /// </summary>
        /// <returns></returns>
        public string ToMessageString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Alarmzeit.ToString("dd.MM.yy HH:mm"));
            sb.AppendLine(Alarmtext);

            return sb.ToString();
        }
    }
}
