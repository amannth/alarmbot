﻿using System;

namespace AlarmBot.Data
{
    public interface IAlarmData
    {
        DateTime Empfangszeit { get; }
        DateTime Alarmzeit { get; }
        string Alarmadresse { get; }
        string Alarmtext { get; }
        string MeldungstextRaw { get; }

        string ToLogString();
        string ToMessageString();
    }
}
