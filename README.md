# AlarmBot
AlarmBot was created as a second alarm way for the members of our German Red Cross organisation.

The idea was to create a simple system to introduce new members in the HvO (Hefler vor Ort) system to compensate the arrival time of the professional ambulance with trained helpers witch are located next to the emergency.

Now we receive the alarm from the rescue coordination center and send the important information via Telegram messenger to the configured usergroups.

In ouer organisation this application runs on a RaspberryPI with .NetCore3 with a attached Swissphone BOSS925 receiver.


## Installation

### Dependencies
Install .NetCore3 on your RaspberryPi
*  download .NetCore3 You need the linux binarys for ARM32 for a raspberryPi <br/>https://dotnet.microsoft.com/download/dotnet-core/3.0<br/>
*  unpack the tar file to /opt/dotnet <br/> `sudo mkdir -p /opt/dotnet && sudo tar zxf dotnet.tar.gz -C /opt/dotnet`
*  set al linkt to dotnet in /usr/local/bin <br/> `sudo ln -s /opt/dotnet/dotnet /usr/local/bin`
*  check installation with command <br/> `dotnet --help`

### Compile and Configure
*  compile project
*  copy the compiled sources to a folder of your choice (e.g. /opt/alarmBot)
*  create the configuration files from the provided excamples and place them in the installation folder of AlarmBot
*  add your Telegram configuration
    1.  create a Telegram bot. See https://core.telegram.org/bots
    2.  insert your Telegram-API key in the AlarmBot configuration file and start the software.
    3.  create a Telegram group for your users and add the bot to the group
    4.  to get the Telegram chatid you can ast the bot with the command `\getid` in the corresponding chatgroup
    5.  now configure your chats in the ChatConfiguration.xml file
    6.  the chatconfiguration can be reloaded with the command `\reloadchats`

### Create Service
*  create a file `/etc/systemd/system/alarmBot.service` and add the following content:
    ```
    [Unit]
    Description=Alarm forwarder
    After=network.target
    StartLimitIntervalSec=0
    
    [Service]
    Type=simple
    Restart=always
    RestartSec=1
    User=pi
    ExecStart=dotnet /opt/alarmBot/AlarmBot.dll
    
    [Install]
    WantedBy=multi-user.target
    ```
*  enable service by executing `systemctl enable alarmBot`
*  start service with `systemctl start alarmBot`
*  to verify that everything is running as expected you can execute `systemctl status alarmBot`
*  you can run it manually with `dotnet ./AlarmBot.dll`
